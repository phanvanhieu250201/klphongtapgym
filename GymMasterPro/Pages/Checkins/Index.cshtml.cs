﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Entities;
using Services.Interfaces;

namespace GymMasterPro.Pages.Checkins
{
    public class IndexModel : PageModel
    {
        private readonly ICheckinService _checkinService;
        private readonly IMembershipService _membershipService; 

        public IndexModel(ICheckinService checkinService, IMembershipService membershipService)
        {
            _checkinService = checkinService;
            _membershipService = membershipService;

        }

        public IList<Checkin> Checkin { get; set; } = default!;

        public async Task OnGetAsync()
        {
            Checkin = await _checkinService.GetCheckins();

            //var membership = await _membershipService.GetMemberships();
            //foreach (var item in membership)
            //{
            //    if (item.MemberId == Checkin.MemberId)
            //    {
            //        Checkin.EndDate = item.EndDate;
            //    }
            //}
        }
    }
}
