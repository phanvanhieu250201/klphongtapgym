﻿using Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Services;
using Services.Interfaces;

namespace GymMasterPro.Pages.Checkins
{
    public class DetailsModel : PageModel
    {
        private readonly ICheckinService _checkinService;
        private readonly IMemberService _memberService;
        private readonly IPlanService _planService;
        private readonly IMembershipService _membershipService;

        public DetailsModel(ICheckinService checkinService,IMemberService memberService, IPlanService planService, IMembershipService membershipService)
        {
            _checkinService = checkinService;
            _memberService = memberService;
            _planService = planService;
            _membershipService = membershipService;

        }

        public Checkin Checkin { get; set; } = default!; 


        public async Task<IActionResult> OnGetAsync(int id)
        {
            if (id == 0)
            {
                return NotFound();
            }

            var checkin = await _checkinService.GetById(id);
            if (checkin == null)
            {
                return NotFound();
            }
            else 
            {
                Checkin = checkin;
                var members = await _memberService.GetMembers();
                foreach (var item in members)
                {
                    if(item.Id == Checkin.MemberId)
                    {
                        Checkin.Member = item;
                    }
                }

                var membership = await _membershipService.GetMemberships();
                foreach (var item in membership)
                {
                    if (item.MemberId == Checkin.MemberId)
                    {
                        var Status = "";
                        if (item.EndDate == null)
                        {
                            Status = "Không đăng ký gói";
                        }
                        else
                        {
                            Status = item.EndDate > DateTime.Now ? "Gói tập còn hạn" : "Hết hạn";
                        }
                        //var Status = item.EndDate == null ? "Không có đăng ký" : item.EndDate > DateTime.Now ? "Gói tập còn hạn" : "Hết hạn";
                        //var Status = item.EndDate > DateTime.Now ? "Gói tập còn hạn" : "Hết hạn";
                        Checkin.Status = Status;
                        Checkin.EndDate = item.EndDate;
                    }
                }

                //var plans = await _planService.GetPlans();
                //foreach (var item in plans)
                //{
                //    if (item.Id == Checkin.Member.Memberships.PlanId)
                //    {
                //        Checkin.Member.Memberships = item;
                //    }
                //}

            }
            return Page();
        }
    }
}
