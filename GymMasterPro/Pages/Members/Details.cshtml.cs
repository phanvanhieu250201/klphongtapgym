﻿using Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Services;
using Services.Interfaces;

namespace GymMasterPro.Pages.Members
{
    public class DetailsModel : PageModel
    {
        private readonly IMemberService _memberService;
        private readonly ITrainerService _trainerService;

        public DetailsModel(IMemberService memberService, ITrainerService trainerService)
        {
            _memberService = memberService;
            _trainerService = trainerService;

        }

        public Member Member { get; set; } = default!;

        public async Task<IActionResult> OnGetAsync(int id)
        {
            if (id == 0 || await _memberService.GetMembers() == null)
            {
                return NotFound();
            }

            var member = await _memberService.GetMemberById(id);
            if (member == null)
            {
                return NotFound();
            }
            else
            {
                Member = member;

                var trainers = await _trainerService.GetTrainers();
                foreach (var item in trainers)
                {
                    if (item.Id == Member.TrainerId)
                    {
                        Member.Trainer = item;
                    }
                }
            }
            return Page();
        }
    }
}
