﻿using Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Services;
using Services.Interfaces;

namespace GymMasterPro.Pages.Members
{
    public class DeleteModel : PageModel
    {
        private readonly IMemberService _memberService;
        private readonly IMembershipService _membershipService;
        private readonly ITrainerService _trainerService;

        public DeleteModel(IMemberService memberService, IMembershipService membershipService,ITrainerService trainerService)
        {
            _memberService = memberService;
            _membershipService = membershipService;
            _trainerService = trainerService;

        }

        [BindProperty]
      public Member Member { get; set; } = default!;

        public async Task<IActionResult> OnGetAsync(int id)
        {
            if (id == 0 || await _memberService.GetMembers() == null)
            {
                return NotFound();
            }

            var member = await _memberService.GetMemberById(id);

            if (member == null)
            {
                return NotFound();
            }
            else 
            {
                Member = member;
            }

            //Membership = membership;

            //foreach (var item in trainer)
            //{
            //    if (item.Id == member.TrainerId)
            //    {
            //        var name = item.FirstName;
            //        Member.Trainer.FirstName = name;
            //    }
            //}

            //var trainers = await _trainerService.GetTrainers();
            //ViewData["TrainerId"] = new SelectList(trainers, "Id", "FirstName");

            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int id)
        {
            if (id == 0 || await _memberService.GetMembers() == null)
            {
                return NotFound();
            }
            var member = await _memberService.GetMemberById(id);

            if (member != null)
            {
                Member = member;
                await _memberService.DeleteAsync(id);
            }

            return RedirectToPage("./Index");
        }
    }
}
