﻿using Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Services;
using Services.Interfaces;

namespace GymMasterPro.Pages.Memberships
{
    public class DetailsModel : PageModel
    {
        private readonly IMembershipService _membershipService;
        private readonly IMemberService _memberService;
        private readonly IPlanService _planService;

        public DetailsModel(IMembershipService membershipService, IMemberService memberService, IPlanService planService)
        {
            _membershipService = membershipService;
            _memberService = memberService;
            _planService = planService;
        }

      public Membership Membership { get; set; } = default!; 

        public async Task<IActionResult> OnGetAsync(int id)
        {
            if (id == 0)
            {
                return NotFound();
            }

            var membership = await _membershipService.GetById(id);
            if (membership == null)
            {
                return NotFound();
            }
            else 
            {
                Membership = membership;
                var members = await _memberService.GetMembers();
                foreach (var item in members)
                {
                    if (item.Id == Membership.MemberId)
                    {
                        Membership.Member = item;
                    }
                }

                var plans = await _planService.GetPlans();
                foreach (var item in plans)
                {
                    if (item.Id == Membership.PlanId)
                    {
                        Membership.Plan = item;
                    }
                }
            }
            return Page();
        }
    }
}
